package com.example.routes

import com.example.models.Customer
import com.example.models.customerStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.customerRouting() {
    route("/customers") {
        get {
            if (customerStorage.isNotEmpty()) call.respond(customerStorage)
            else call.respondText("No customers found.", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            /*
                0.0.0.0:8080/customers/{id?}
                Mirar que existeixi el paràmetre -> Missatge i BAD REQUEST
                Filtrar por id:
                    Si existe -> devuelve cliente
                    Si no -> Mensaje y NOT FOUND
             */
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in customerStorage) {
                if (customer.id == id) return@get call.respond(customer)
            }
            call.respondText(
                "Customer with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        post {
            val customer = call.receive<Customer>()
            customerStorage.add(customer)
            call.respondText("Customer stored correctly", status = HttpStatusCode.Created)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val custToUpd = call.receive<Customer>()
            for (customer in customerStorage) {
                if (customer.id == id) {
                    customer.firstName = custToUpd.firstName
                    customer.lastName = custToUpd.lastName
                    customer.email = custToUpd.email
                    return@put call.respondText(
                        "Customer with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Customer with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in customerStorage) {
                if (customer.id == id) {
                    customerStorage.remove(customer)
                    return@delete call.respondText("Customer removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Customer with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
    }
}
